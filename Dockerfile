FROM python:3-alpine AS base

LABEL maintainer="André Nishitani <atoshio25@gmail.com>"

COPY dev_scraper /app/dev_scraper
COPY ./run.py ./tests ./requirements.txt /app/
WORKDIR /app

RUN pip install -r requirements.txt 

EXPOSE 80

ENTRYPOINT [ "gunicorn", "app:app", "-b 0.0.0.0:80" ]
