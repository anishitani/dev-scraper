import unittest
from unittest.mock import patch

from dev_scraper import app
from dev_scraper.entities.feed import Feed
from dev_scraper.services.feeds_service import FeedsService
from flask import json


class TestFeedApi(unittest.TestCase):
    def setUp(self):
        self.client = app.test_client()

    def test_get_feeds_api(self):
        expected = [
            Feed(title="Hello World 01", description="Message 01"),
            Feed(title="Hello World 02", description="Message 02")
        ]

        with patch.object(FeedsService, 'get_feeds', return_value=expected):
            response = self.client.get('/feeds')
            result = json.loads(response.get_data())

            self.assertEqual(response.status_code, 200,
                             "Expecting a success code 200")
            self.assertEqual(len(expected), len(result),
                             "Expected and result should have the same size")
            self.assertListEqual(
                [d.__dict__ for d in expected], result, "Expect all items to match the response")
