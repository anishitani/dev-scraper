import unittest
from unittest.mock import Mock

from dev_scraper.entities.feed import Feed
from dev_scraper.entities.feed_scraper import FeedsScraper
from dev_scraper.services.feeds_service import FeedsService


class TestFeedService(unittest.TestCase):
    def test_get_feed(self):
        mock_01 = Mock()
        mock_01.return_value = [
            Feed(title="Hello World 01", description="Message 01")]
        feeder_01 = FeedsScraper()
        feeder_01.get_feeds = mock_01

        mock_02 = Mock()
        mock_02.return_value = [
            Feed(title="Hello World 02", description="Message 02")]
        feeder_02 = FeedsScraper()
        feeder_02.get_feeds = mock_02

        service = FeedsService([feeder_01, feeder_02])

        feeds = service.get_feeds()
        expected = [
            Feed(title="Hello World 01", description="Message 01"),
            Feed(title="Hello World 02", description="Message 02")
        ]

        self.assertEqual(len(feeds), len(expected), "Expected and result should have the same size")

        self.assertListEqual(
            feeds, expected, "Expect all items to match the response")

    def test_empty_get_feed(self):
        mock_01 = Mock()
        mock_01.return_value = []
        feeder_01 = FeedsScraper()
        feeder_01.get_feeds = mock_01

        mock_02 = Mock()
        mock_02.return_value = []
        feeder_02 = FeedsScraper()
        feeder_02.get_feeds = mock_02

        service = FeedsService([feeder_01, feeder_02])

        feeds = service.get_feeds()
        
        self.assertEqual(len(feeds), 0, "Expect list to be empty")
