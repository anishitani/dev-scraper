import unittest

import requests_mock
from dev_scraper.entities.feed_source import FeedSource
from dev_scraper.external.feed_page_fetcher import FeedPageFetcher


class TestFeedPageFetcher(unittest.TestCase):
    def setUp(self):
        self.service = FeedPageFetcher()
        self.url = "http://awesome.src"
        self.source = FeedSource(self.url)

    def test_fetch_page_given_good_source(self):
        with requests_mock.Mocker() as m:
            m.get(self.url, text="Hello World!")
            result = self.service.fetch_page(self.source)
            self.assertEqual(result, "Hello World!")

    def test_fetch_page_given_bad_source(self):
        with requests_mock.Mocker() as m:
            m.get(self.url, status_code=404)
            result = self.service.fetch_page(self.source)
            self.assertEqual(result, str())
