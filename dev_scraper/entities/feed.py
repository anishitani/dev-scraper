class Feed:
    title: str
    description: str

    def __init__(self, title: str = None, description: str = None) -> None:
        self.title = title
        self.description = description

    def __eq__(self, o) -> bool:
        return type(o) is Feed and \
            self.title == o.title and \
            self.description == o.description
