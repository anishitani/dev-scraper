from typing import List

from dev_scraper.entities.feed import Feed
from dev_scraper.entities.feed_scraper import FeedsScraper
from dev_scraper.entities.feed_source import FeedSource
from dev_scraper.external.feed_page_fetcher import FeedPageFetcher
from bs4 import BeautifulSoup as bs


class GeekForGeeksScraper(FeedsScraper):
    source: FeedSource
    fetcher: FeedPageFetcher

    def __init__(self) -> None:
        self.source = FeedSource(url="https://www.geeksforgeeks.org/")
        self.fetcher = FeedPageFetcher()

    def get_feeds(self) -> List[Feed]:
        feeds = []
        page = self.fetcher.fetch_page(self.source)
        soup = bs(page, 'html.parser')
        articles = soup.find_all('article')
        for article in articles:
            feed = Feed()
            feed.title = article.find('header').find('a').get_text()
            feed.description = article.find(
                'div', class_='entry-summary').find('p').get_text()
            feeds.append(feed)
        return feeds
