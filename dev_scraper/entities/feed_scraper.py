from typing import List

from dev_scraper.entities.feed import Feed


class FeedsScraper(object):
    def __init__(self) -> None:
        pass

    def get_feeds(self) -> List[Feed]:
        pass
