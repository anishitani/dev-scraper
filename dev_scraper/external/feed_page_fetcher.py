import requests
from dev_scraper.entities.feed_source import FeedSource


class FeedPageFetcher(object):
    def fetch_page(self, source: FeedSource) -> str:
        response = requests.get(source.url)
        if response.ok:
            return response.text
        return str()
