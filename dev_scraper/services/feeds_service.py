from typing import List

from dev_scraper.entities.feed import Feed
from dev_scraper.entities.feed_scraper import FeedsScraper


class FeedsService():
    def __init__(self, scrapers: List[FeedsScraper]):
        self.scrapers = scrapers

    def get_feeds(self) -> List[Feed]:
        feeds = []
        for scraper in self.scrapers:
            feeds = feeds + scraper.get_feeds()
        return feeds
