from dev_scraper.entities.geek_for_geeks_scraper import GeekForGeeksScraper
from dev_scraper.services.feeds_service import FeedsService
from flask.blueprints import Blueprint
from flask.json import jsonify

FEEDERS = [
    GeekForGeeksScraper()
]

feeds_api = Blueprint('Feeds', __name__)
service = FeedsService(FEEDERS)


@feeds_api.route('', methods=['GET'])
def get_feeds():
    feeds = service.get_feeds()
    return jsonify([feed.__dict__ for feed in feeds]), 200, {'Content-Type': 'application/json'}
