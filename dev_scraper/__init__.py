from dotenv import load_dotenv
from flask import Flask

from dev_scraper.api.feeds_api import feeds_api

# Load environment variables
load_dotenv(verbose=False)

# initialize the application
app = Flask(__name__, instance_relative_config=True)


app.register_blueprint(feeds_api, url_prefix='/feeds')
